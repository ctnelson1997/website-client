import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BadgerSignatureComponent } from './badger-signature/badger-signature.component';
import { OilComponent } from './oil/oil.component';
import { LandingComponent } from './landing/landing.component';
import { TabnabComponent } from './tabnab/tabnab.component';

@NgModule({
  declarations: [
    AppComponent,
    BadgerSignatureComponent,
    OilComponent,
    LandingComponent,
    TabnabComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
