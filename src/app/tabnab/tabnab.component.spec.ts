import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabnabComponent } from './tabnab.component';

describe('TabnabComponent', () => {
  let component: TabnabComponent;
  let fixture: ComponentFixture<TabnabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabnabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabnabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
