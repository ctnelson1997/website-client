import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tabnab',
  templateUrl: './tabnab.component.html',
  styleUrls: ['./tabnab.component.scss']
})
export class TabnabComponent implements OnInit {

  private static readonly DEFAULT_TARGET = 'https://www.youtube.com/watch?v=dQw4w9WgXcQ';

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      const target = params['target'] ? params['target'] : TabnabComponent.DEFAULT_TARGET;
      window.opener.location = target;
    })
  }

}
