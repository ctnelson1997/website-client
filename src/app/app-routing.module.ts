import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OilComponent } from './oil/oil.component';
import { LandingComponent } from './landing/landing.component';
import { TabnabComponent } from './tabnab/tabnab.component';


const routes: Routes = [
  {path: '', component: LandingComponent},
  {path: 'oil', component: OilComponent},
  {path: 'tabnab', component: TabnabComponent},
  {path: '**', component: LandingComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
