import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BadgerSignatureComponent } from './badger-signature.component';

describe('BadgerSignatureComponent', () => {
  let component: BadgerSignatureComponent;
  let fixture: ComponentFixture<BadgerSignatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BadgerSignatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BadgerSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
